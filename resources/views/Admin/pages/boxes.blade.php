@extends('Admin.layouts.master_layout')
@section('content')
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Boxes</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Boxes</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Total ({{$boxes->total()}})  </h4>
                    <div id="add-box" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel">Action</h4> 
                                    <a class="close pull-right" data-dismiss="modal" aria-hidden="true">×</a></div>
                                <div class="modal-body">
                                    <div name='box-insert'>
                                        <div class="form-group">
                                        <label class="control-label">Box Name</label>
                                        <input type="text" id="name" disabled="disabled" class="form-control" > </div>
                                        <div class="form-group">
                                        <label class="control-label">Subscription Charges</label>
                                        <input type="number" id="sub_chrgs" required="true" class="form-control" > </div>
                                        <div class="form-group">
                                        <label class="control-label">Maintenance Charges</label>
                                        <input type="number" id="maint_chrgs" required="true" class="form-control" > </div> </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-info waves-effect" name="btnupdate" >Update</button>
                                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cancel</button>
                                    </div>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered" name="boxes">
                            <thead>
                                <tr>
                                    <th>Sl. No</th>
                                    <th>Box</th>
                                    <th>Subscription Charges</th>
                                    <th>Maintenance Charges</th>
                                    <th class="text-nowrap">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @php $i=1; @endphp 
                            @foreach ($boxes as $box)
                                <tr id="{{$box->id}}">
                                    <td>{{ (( $boxes->currentPage() - 1) * 10 ) + $i++ }}</td>
                                    <td>{{$box->name}}</td>
                                    <td><i class="fa fa-inr"></i> {{$box->subscription}}</td>
                                    <td><i class="fa fa-inr"></i> {{$box->maintenance}}</td>
                                    <td class="text-nowrap">
                                        <a href="javascript:void(0);" name="btnRowEdit" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>
                                    </td>
                                </tr>
                            @endforeach
                            @if (!$boxes->count())
                            <tr><td  > No boxes found</td></tr>
                            @endif
                            </tbody>
                        </table>
                        <ul class="pagination pagination-sm no-margin pull-right"></ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src='/admin/jscontrols/boxes.js'></script>
<script>var currentpage = 1;</script>
<script> 
  $('.pagination').twbsPagination({
      totalPages: Math.ceil("{{ $boxes->total() / 10 }}"),
      startPage:parseInt("{{ $boxes->currentPage() }}"),
      initiateStartPageClick:false,
      visiblePages: 10,
      onPageClick: function (event, page) {
        var getdata = {};
        getdata['page'] = page;
        var display = "";
        // $.loader.start(); 
        $.get('/superadmin/boxes/get-data',getdata,function(response){
          var display = "";
          for(var i=0; i<response.data.length; i++){
            // $.loader.stop();
             display += '<tr id=' + response.data[i].id + '><td >' + 
                        parseInt(parseInt(i + 1) + parseInt((page - 1) * 10)) +'</td> '+
                        '<td > '+response.data[i].name+'</td>' +
                        '<td > '+response.data[i].subscription+'</td>' +
                        '<td > '+response.data[i].maintenance+'</td>' +
                        '<td >'+
                        '<a href="javascript:void(0);" name="btnRowEdit" data-toggle="tooltip" data-original-title="Edit"> <i class="fa fa-pencil text-inverse m-r-10"></i></a>'+
                        '</td>'+
                        '</tr>';
          }

          $('table[name=boxes] tbody').html(display);
        
      },'json');

        history.pushState('', '', '/superadmin/boxes?page=' + page);
        currentpage = page;
      }
  });
</script>
@endsection