
var myApp = angular.module('TapmiAdmin', ['ngRoute']); 

myApp.controller("BillController", ['$scope', '$http', function($scope, $http) {
      
   $scope.loadinfo = function(page, $cond='') {
        $http.get('/superadmin/getbills?page='+page+(($cond) ? ('&'+$cond) : ''))
            .then(function(res) {
                  $scope.displaybills = res.data.data;
                  $scope.page = (page-1)*res.data.per_page;
                  $('[name=pagination]').twbsPagination({
			        totalPages: res.data.last_page,
			        visiblePages: 7,
			        onPageClick: function (event, page) {
			        $scope.loadinfo(page);
			        }
			     });   
        

        });
    }
 
   $scope.loadinfo(1);
  
      
	
}]);
