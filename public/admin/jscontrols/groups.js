$(function() {

    $.groups = {
        init: function(callback) {
            $(document).on('click', '[name="btnRowEdit"]', function() {
                $.groups.edit($(this));
            });

            $(document).on('click', '[name=btnupdate]', function() {
                $.groups.update($(this));
            });
            
            $(document).on('click', '[name=btncancel]', function() {
                $.groups.back($(this));
            });

            $(document).on('click', '[name="showForm"]', function() {
                $.groups.showForm();
            });

            $(document).on('click', '[name=btninsert]', function() {
                $.groups.insert($(this));
            });

            $(document).on('click', '[name=btnRowDelete]', function() {
                $.groups.delete($(this));
            });
        },
        display: function(offset) {
            offset = parseInt(offset) ? offset : '1';
            //display companys
            // $.loader.start();
            
            $.get('/superadmin/groups/get-data?page=' + offset, function(response) {
                // $.loader.stop();
                var display = "";
                
                for (var i = 0; i < response.data.length; i++) {
                    display += '<tr id=' + response.data[i].id + '><td >' + 
                        parseInt(parseInt(i + 1) + parseInt((offset - 1) * 10)) +
                        '</td><td > '+response.data[i].name+'</td><td >' +
                        response.data[i].created_at + '</td><td ><a href="javascript:void(0);" name="btnRowEdit" data-toggle="tooltip" data-original-title="Edit"><i class="fa fa-pencil text-inverse m-r-10"></i></a>'+
                        '&nbsp;<a href="javascript:void(0);" name="btnRowDelete" class="text-danger" data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-close text-danger"></i></a>' +
                        '</td>'+
                        '</tr>';
                }

                display = display == "" ? '<tr><th colspan=4 class="text-center">No groups found </th></tr>' : display;

                // alert(display);
                $('[name="groups"] tbody').html(display);
            }, 'json');
        },
        query_params: function(name, url) {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        },
        update: function(ths) {
            var id = ths.val();

            var proceed = true;

            $('[name=group-insert] [required=true]:visible').filter(function() {
                if ($.trim(this.value) == "") {
                    $(this).css('border-color', 'red');
                    proceed = false;
                }
            });

            if (proceed) {
                var postdata = new FormData();
                postdata.append('name', $('#name').val());
                postdata.append('_token', $('meta[name="csrf_token"]').attr('content'));

                // $.loader.start();

                $.ajax({
                    url: "/superadmin/groups/update/" + id,
                    async: !1,
                    type: "POST",
                    dataType: "json",
                    contentType: false,
                    cache: false,
                    processData: false,
                    data: postdata,
                    success: function(response) {
                        // $.loader.stop();
                        if (response.status) {
                            $('#add-group').modal('hide');
                            $.groups.display($.groups.query_params('page'));
                            
                            var display = response.message;

                            // $.confirm({
                            //     title: '',
                            //     content: display,
                            //     type: 'blue',
                            //     typeAnimated: true,
                            //     buttons: {
                            //         tryAgain: {
                            //             text: 'ok',
                            //             btnClass: 'btn-primary',
                            //             action: function() {
                            //                 // $('#down').animatescroll();
                            //                 setTimeout(function() {
                            //                     $('form[name=tip-insert]').get(0).reset();
                            //                     $('[name="tip-display"]').show();
                            //                     $('[name="tip-edit"]').hide();
                            //                     $.groups.display($.groups.query_params('page'));
                            //                 }, 1000)
                            //             }
                            //         },
                            //         // close: function () {
                            //         // }
                            //     }
                            // });

                        } else {
                            // var display = "";
                            // if ((typeof response.message) == 'object') {
                            //     $.each(response.message, function(key, value) {
                            //         display = key.replace("_", " ").toUpperCase() + ' ' + value[0];
                            //     });
                            // } else {
                            //     display = response.message;
                            // }

                            // $.confirm({
                            //     title: '',
                            //     content: display,
                            //     type: 'red',
                            //     typeAnimated: true,
                            //     buttons: {
                            //         tryAgain: {
                            //             text: 'Error',
                            //             btnClass: 'btn-red',
                            //             action: function() {}
                            //         },
                            //         close: function() {}
                            //     }
                            // });

                        }
                    }
                });
            }
        },
        showForm: function(ths) {
            $('#add-group').find('#myModalLabel').text('Add New Group');

            $('#add-group').find('#name').val('');
            $('#add-group').find('[name=btninsert]').show();
            $('#add-group').find('[name=btnupdate]').attr('value', '').hide();
            $('#add-group').modal('show');
        },
        edit: function(ths) {
            var id = ths.closest('tr').attr('id');
            // $.loader.start();

            $.ajax({
                url: '/superadmin/groups/view/' + id,
                type: "GET",
                dataType: "json",

                success: function(response) {
                    // $.loader.stop();
                    $('#add-group').find('#myModalLabel').text('Edit Group');

                    $('#add-group').find('#name').val(response.name);
                    $('#add-group').find('[name=btninsert]').hide();
                    $('#add-group').find('[name=btnupdate]').attr('value', response.id).show();
                    $('#add-group').modal('show');
                },
                error: function() { /* error code goes here*/ }
            });
        },
        back: function($this) {
            $('form[name=tip-insert]').get(0).reset();
            $('[name="tip-display"]').show();
            $('[name="tip-edit"]').hide();
        },
        delete: function(ths) {
            var id = ths.closest('tr').attr('id');
            var postdata = new FormData();
            postdata.append('id', id);
            postdata.append('_token', $('meta[name="csrf_token"]').attr('content'));
            swal({   
                title: "Are you sure?",   
                text: "You will not be able to recover this group",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Yes, delete it!",   
                closeOnConfirm: false 
            }, function(){
                $.ajax({
                    url: '/superadmin/groups/delete',
                    type: 'POST',
                    async: !1,
                    dataType: "json",
                    contentType: false, // The content type used when sending data to the server.
                    cache: false, // To unable request pages to be cached
                    processData: false,
                    data: postdata,
                    success: function(response) {
                        // $.loader.stop();
                        if (response.status) {
                            $('[name="groups"] tbody tr#' + id).css('background-color', 'rgb(181, 117, 117)').hide(1000);
                            setTimeout(function() {
                                if ($('[name="groups"] tbody tr:visible').length == 0) {
                                    var display = "<tr><th colspan=4 style='color:red'>No Groups Found </th></tr>";
                                    $('[name="groups"] tbody').html(display);
                                }
                            }, 1001);
                            // display = response.message;
                            display = "Group has been deleted.";

                            swal("Success!", display, "success");
                        } else {
                            var display = "Some error occured";
                            if ((typeof response.message) == 'object') {
                                $.each(response.message, function(key, value) {
                                    display =  value[0];
                                });
                            } else {
                                display = response.message;
                            }
                            swal(display);
                        }

                    }
                });
            });
            
        },
        insert: function(ths, callback) {
            var proceed = true;
            $('[name=group-insert] [required=true]').filter(function() {
                if ($.trim(this.value) == "") {
                    $(this).css('border-color', 'red');
                    proceed = false;
                }
            });

            if (proceed) {
                var postdata = new FormData();
                postdata.append('name', $('#name').val());
                postdata.append('_token', $('meta[name="csrf_token"]').attr('content'));

                // $.loader.start();

                $.ajax({
                    url: "/superadmin/groups/add",
                    async: !1,
                    type: "POST",
                    dataType: "json",
                    contentType: false, // The content type used when sending data to the server.
                    cache: false, // To unable request pages to be cached
                    processData: false,
                    data: postdata,
                    success: function(response) {
                        // $.loader.stop();

                        if (response.status) {
                            $.groups.display($.groups.query_params('page'));
                            $('#add-group').modal('hide');
                            // var display = response.message;
                            // $.confirm({
                            //     title: '',
                            //     content: display,
                            //     type: 'green',
                            //     typeAnimated: true,
                            //     buttons: {
                            //         tryAgain: {
                            //             text: 'ok',
                            //             btnClass: 'btn-green',
                            //             action: function() {
                            //                 $('form[name=tip-insert]').get(0).reset();
                            //                 $('[name="tip-display"]').show();
                            //                 $('[name="tip-edit"]').hide();
                            //                 // $('#down').animatescroll();
                            //                 setTimeout(function() {
                            //                     $.groups.display($.groups.query_params('page'));
                            //                 }, 1000);

                            //             }
                            //         },
                            //         close: function() {}
                            //     }
                            // });

                        } else {
                            // var display = "";
                            // if ((typeof response.message) == 'object') {
                            //     $.each(response.message, function(key, value) {
                            //         display = value[0]; //fetch oth(first) error.

                            //     });
                            // } else {
                            //     display = response.message;
                            // }
                            // $.confirm({
                            //     title: '',
                            //     content: display,
                            //     type: 'red',
                            //     typeAnimated: true,
                            //     buttons: {
                            //         close: function() {}
                            //     }
                            // });
                        }
                    }
                });
            }
        }
    }
    $.groups.init();
}(jQuery));