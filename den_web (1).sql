-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 14, 2018 at 02:42 PM
-- Server version: 5.7.22-0ubuntu0.16.04.1
-- PHP Version: 7.0.28-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `den_web`
--

-- --------------------------------------------------------

--
-- Table structure for table `billings`
--

CREATE TABLE `billings` (
  `id` int(11) UNSIGNED NOT NULL,
  `lco_id` int(11) NOT NULL,
  `box_type` int(11) NOT NULL,
  `box_qty` int(11) NOT NULL,
  `month` varchar(10) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `subscription` int(11) NOT NULL,
  `maintenance` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bill_status_types`
--

CREATE TABLE `bill_status_types` (
  `id` int(11) UNSIGNED NOT NULL,
  `value` tinyint(1) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bill_status_types`
--

INSERT INTO `bill_status_types` (`id`, `value`, `name`) VALUES
(5, 0, 'Pending'),
(6, 1, 'Partial Payment'),
(7, 2, 'Full Payment');

-- --------------------------------------------------------

--
-- Table structure for table `boxes`
--

CREATE TABLE `boxes` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `subscription` decimal(10,2) NOT NULL,
  `maintenance` decimal(10,2) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `boxes`
--

INSERT INTO `boxes` (`id`, `name`, `subscription`, `maintenance`, `created_at`, `updated_at`) VALUES
(1, 'SD BOXES', '10.00', '2.00', '2018-05-14 00:00:00', '2018-05-14 14:07:25'),
(2, 'HD-SD BOXES', '21.50', '6.75', '2018-05-14 00:00:00', '2018-05-14 14:08:55'),
(3, 'HD-HD BOXES', '0.00', '0.00', '2018-05-14 00:00:00', '2018-05-14 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cable_operators`
--

CREATE TABLE `cable_operators` (
  `id` int(11) UNSIGNED NOT NULL,
  `lco_num` varchar(50) NOT NULL,
  `gst_num` varchar(50) DEFAULT NULL,
  `group_id` int(11) UNSIGNED DEFAULT NULL,
  `name` varchar(300) DEFAULT NULL,
  `mobile` varchar(30) DEFAULT NULL,
  `address` text,
  `email` varchar(200) DEFAULT NULL,
  `city` varchar(200) DEFAULT NULL,
  `district` varchar(200) DEFAULT NULL,
  `pin_code` varchar(30) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cable_operators`
--

INSERT INTO `cable_operators` (`id`, `lco_num`, `gst_num`, `group_id`, `name`, `mobile`, `address`, `email`, `city`, `district`, `pin_code`, `created_at`, `updated_at`) VALUES
(3, 'JVBLR07', '22AAAAA0000A1Z5', 23, 'DEN UCN Network India PVT LTD', '-', '-', '-', '-', '-', '576213', '2018-05-11 15:11:14', '2018-05-14 10:46:31'),
(4, 'LCOBL782', NULL, 24, 'World Wide Vision', '9886540000', '-', '-', '-', '-', '789456', '2018-05-11 15:11:14', '2018-05-11 15:11:14'),
(5, 'LCOBL1322', NULL, 25, 'Durga Cable Service', '8861860269', '-', '-', '-', '-', '789456', '2018-05-11 15:11:14', '2018-05-11 15:11:14'),
(6, 'LCOBL2233', NULL, 26, 'Jeethendri Cable Network', '8746863435', '-', '-', '-', '-', '789456', '2018-05-11 15:11:14', '2018-05-11 15:11:14'),
(7, 'LCOBL1037', NULL, 27, 'Rashmi Cable', '9448109605', '-', '-', '-', '-', '784452', '2018-05-11 15:11:14', '2018-05-11 15:11:14'),
(20, 'LCOBL1055', NULL, 22, 'Shri Parashakthi Network', '9448022524', '-', '-', '-', '-', '-', '2018-05-11 15:11:14', '2018-05-11 15:11:14'),
(21, 'JVBLR06', NULL, 23, 'DEN UCN Network India PVT LTD', '-', '-', '-', '-', '-', '576213', '2018-05-11 15:11:14', '2018-05-11 15:11:14'),
(22, 'LCOBL785', NULL, 24, 'World Wide Vision', '9886540000', '-', '-', '-', '-', '789456', '2018-05-11 15:11:14', '2018-05-11 15:11:14'),
(23, 'LCOBL1321', NULL, 25, 'Durga Cable Service', '8861860269', '-', '-', '-', '-', '789456', '2018-05-11 15:11:14', '2018-05-11 15:11:14'),
(24, 'LCOBL2234', NULL, 26, 'Jeethendri Cable Network', '8746863435', '-', '-', '-', '-', '789456', '2018-05-11 15:11:14', '2018-05-11 15:11:14'),
(25, 'LCOBL1031', NULL, 27, 'Rashmi Cable', '9448109605', '-', '-', '-', '-', '784452', '2018-05-11 15:11:14', '2018-05-11 15:11:14');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(500) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `created_at`, `updated_at`) VALUES
(22, 'Bhaskar shet', '2018-05-11 15:11:13', '2018-05-11 15:11:13'),
(23, 'Den Networks Ltd.', '2018-05-11 15:11:14', '2018-05-11 15:11:14'),
(24, 'ERIC', '2018-05-11 15:11:14', '2018-05-11 15:11:14'),
(25, 'GURU', '2018-05-11 15:11:14', '2018-05-11 15:11:14'),
(26, 'ICE Surarthkal', '2018-05-11 15:11:14', '2018-05-11 15:11:14'),
(27, 'Jayanth Maravanthe', '2018-05-11 15:11:14', '2018-05-11 15:11:14'),
(28, 'Bhaskar shet', '2018-05-11 15:11:13', '2018-05-11 15:11:13'),
(29, 'Den Networks Ltd.', '2018-05-11 15:11:14', '2018-05-11 15:11:14'),
(30, 'ERIC', '2018-05-11 15:11:14', '2018-05-11 15:11:14'),
(31, 'GURU', '2018-05-11 15:11:14', '2018-05-11 15:11:14'),
(32, 'ICE Surarthkal', '2018-05-11 15:11:14', '2018-05-11 15:11:14'),
(33, 'Jayanth Maravanthe', '2018-05-11 15:11:14', '2018-05-11 15:11:14');

-- --------------------------------------------------------

--
-- Table structure for table `lco_boxes`
--

CREATE TABLE `lco_boxes` (
  `id` int(11) UNSIGNED NOT NULL,
  `box_id` int(11) UNSIGNED NOT NULL,
  `box_qty` int(11) UNSIGNED NOT NULL,
  `lco_id` int(11) UNSIGNED NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(11) UNSIGNED NOT NULL,
  `txn_id` varchar(100) NOT NULL,
  `billing_id` int(11) UNSIGNED NOT NULL,
  `mode` tinyint(1) NOT NULL,
  `amount` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '1 - successful paymnent,  0 - rollback payment (eg. cheque return) ',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payment_details`
--

CREATE TABLE `payment_details` (
  `id` int(11) UNSIGNED NOT NULL,
  `payment_id` int(11) NOT NULL,
  `meta_data` text NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '0 - Mobile Not Verified, 1 - Active',
  `power` tinyint(1) UNSIGNED NOT NULL COMMENT '1 - Admin, 2- Others',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `mobile`, `password`, `status`, `power`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Den Admin', 'admin@den.com', '9999999999', '$2y$10$gVoMgBorSE0OZ/54.qzUbOAb1BcXVW/Us7p30PF/HJlxQC1EU3QVa', 1, 1, 'zfbWoO6KzcLxXq9DxD5qEdFuDoSn5Etz1TYC6Hsd3cfvlJPCcRl5uoSpH553', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `billings`
--
ALTER TABLE `billings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bill_status_types`
--
ALTER TABLE `bill_status_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `boxes`
--
ALTER TABLE `boxes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cable_operators`
--
ALTER TABLE `cable_operators`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `lco_num` (`lco_num`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lco_boxes`
--
ALTER TABLE `lco_boxes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_details`
--
ALTER TABLE `payment_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mobile` (`mobile`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `billings`
--
ALTER TABLE `billings`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bill_status_types`
--
ALTER TABLE `bill_status_types`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `boxes`
--
ALTER TABLE `boxes`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `cable_operators`
--
ALTER TABLE `cable_operators`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `lco_boxes`
--
ALTER TABLE `lco_boxes`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `payment_details`
--
ALTER TABLE `payment_details`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
